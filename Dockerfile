FROM voidtek/ci-android-sdk:latest

ARG FLUTTER_VERSION=1.9.1+hotfix.4

RUN wget --quiet --output-document=flutter-sdk.tar.xz https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v$FLUTTER_VERSION-stable.tar.xz \
  && tar -xf flutter-sdk.tar.xz \
  && rm flutter-sdk.tar.xz

RUN export PATH=$PATH:$PWD/flutter/bin
